from setuptools import setup

setup(name='sample',
      version='0.2',
      description='Coding sample package',
      url='https://bitbucket.org/richard_waddington/sample',
      author='Richard B. Waddington',
      author_email='rich.waddington@gmail.com',
      license='BSD-3-Clause',
      packages=['sample'],
      zip_safe=False)
