This is supplied as a runnable code sample, developed on
ubuntu 16.04/18.04 with python v3.6.2.
It has been tested onubuntu 16.04 and Windows 10.

INSTALLATION
============
Open a terminal window

Clone the repository
```bash
git clone https://bitbucket.org/richard_waddington/sample
```

Install the sample package
```bash
cd LOCAL_REPO/sample
pip install sample
```

DEMO INSTRUCTIONS
=================

Start the Firmware
```bash
cd LOCAL_REPO/sample
python firmware.py
# every two seconds, a sensor read is displayed
```
Open another terminal window and...
```bash
cd LOCAL_REPO/client
python client.py GET-SENSOR 0
# the value of sensor 0 is displayed

python client.py SET-SENSOR 0 42.0
# in the firmware window, an error is displayed
```
When finished, control-c will terminate firmware.py.

PACKAGE CONTENTS
=============
firmware.py
----------

    Dummy firmware for a mythical device. The device monitors a
    sensor and displays its value. It displays an error if the
    value read is too high or too low.

sample/hal.py
-------------
    Exposes a list of sensors

sample/sensors.py
-----------------
    Implements a MockSensor. In a more sofisticated device this
    would be the base class for various different types of
    sensors.

sample/tst_framework/producer.py
--------------------------------
    Provides a stream of fake data to the sensors - the "happy
    path"

sample/tst_framework/tst_framework.py
------------------------------------
    Implements the hooks for test framework to read sensor values
    or modify sensor values allowing for the "firmware" behavior
    to be tested. Note: creates a TCP server at localhost:9999

client/client.py
----------------
    Based on command line input, sends a command to the test
    framework on the device and displays the response.
