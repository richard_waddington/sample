#!/usr/bin/env python3
import sys
import traceback
import time
import sample
from sample.tst_framework import TstFramework


class Firmware(object):
    def __init__(self):
        self.hal = None

    def boot(self):
        """ actions taken at boot/reboot """
        print('Booting...')
        print('Initializing Test Framework')
        framework = TstFramework()
        framework.set_server()
        framework.start_server()
        self.hal = sample.hal.Hal(framework)

    def run(self):
        """ Main firmware loop """
        print('Running...')
        while True:
            try:
                for sensor in self.hal.sensors:
                    value = sensor.read()
                    msg = 'Sensor ID: {}, {}'.format(sensor.sensor_id, value)
                    if value < 36.0:
                        msg += ' ERROR: value too low'
                    elif value > 39.0:
                        msg += ' ERROR: value too high'
                    print(msg)
                    time.sleep(2)

            except Exception as ex:
                print('Exception: ', type(ex).__name__)
                traceback.print_tb(sys.exc_info()[-1], file=sys.stdout)
                # Restart if an exception occurred
                self.boot()


if __name__ == '__main__':
    fw = Firmware()
    fw.boot()
    fw.run()  # Never returns
