#!/usr/bin/env python3
import socket
import sys

HOST, PORT = "localhost", 9999

data = ''
if len(sys.argv) >= 2:
    data += sys.argv[1]
if len(sys.argv) >= 3:
    data += ',' + sys.argv[2]
if len(sys.argv) == 4:
    data += ',' + sys.argv[3]
data += '\n'

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    sock.sendall(bytes(data, 'utf-8'))

    # Receive data from the server and shut down
    received = sock.recv(1024)
finally:
    sock.close()

print("Sent:     {}".format(data))
print("Received: {}".format(received.decode("utf-8")))
