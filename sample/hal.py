class Hal(object):
    def __init__(self, tst_framework=None):
        self.tst_framework = tst_framework
        self._init_sensors()

    def _init_sensors(self):
        print('Initializing sensors')
        if self.tst_framework:
            self.sensors = self.tst_framework.sensors
        else:
            # Implement actual hardware sensors
            self.sensors = None
