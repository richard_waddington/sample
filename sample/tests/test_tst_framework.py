from unittest import TestCase
from tst_framework import TstFramework

NAK = bytes('NAK:', 'utf-8')


class TestTstFramework(TestCase):
    def setUp(self):
        self.framework = TstFramework()

    def test_get_sensor_out_of_range_low(self):
        result = self.framework.do_command(b'GET-SENSOR,-1')
        self.assertEqual(NAK, result[:4])

    def test_get_sensor_out_of_range_high(self):
        result = self.framework.do_command(b'GET-SENSOR,100')
        self.assertEqual(NAK, result[:4])

    def test_get_sensor_command(self):
        expected = b'ACK: GET-SENSOR,0,36.5\n'
        result = self.framework.do_command(b'GET-SENSOR,0')
        self.assertEqual(expected, result)

    def test_set_sensor_int(self):
        expected = b'ACK: SET-SENSOR,0,37\n'
        result = self.framework.do_command(b'SET-SENSOR,0,37')
        self.assertEqual(expected, result)

    def test_set_sensor_float(self):
        expected = b'ACK: SET-SENSOR,0,37.0\n'
        result = self.framework.do_command(b'SET-SENSOR,0,37.0')
        self.assertEqual(expected, result)

    def test_invalid_command(self):
        result = self.framework.do_command(b'INVALID-COMMAND,100,82')
        self.assertEqual(NAK, result[:4])
