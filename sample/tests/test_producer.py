from unittest import TestCase
from tst_framework import SensorProducer

class TestProducer(TestCase):
    def setUp(self):
        self.producer = SensorProducer()

    def test_producer(self):
        self.assertIsNotNone(self.producer._default_values)
        self.assertIsNotNone(self.producer._values)
        self.assertIsNotNone(self.producer._lock)
        self.assertEqual(0, self.producer._next)

    def test_producer_value(self):
        for x in self.producer._values:
            self.assertEqual(x, self.producer.value)
        self.assertEqual(36.5, self.producer.value)

    def test_set_value(self):
        self.producer.value = 100
        self.assertEqual(100, self.producer.value)
        self.assertEqual(36.6, self.producer.value)
