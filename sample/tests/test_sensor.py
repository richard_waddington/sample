from unittest import TestCase
from sensors import MockSensor
from tst_framework import SensorProducer

class TestFramework(TestCase):
    def setUp(self):
        self.sensor = MockSensor(1)

    def test_sensor_id(self):
        self.assertEqual(1, self.sensor.sensor_id)

    def test_sensor_producer(self):
        self.assertIsNone(self.sensor.producer)

    def test_sensor_default_value(self):
        self.assertEqual(10, self.sensor.def_value)

    def test_sensor_read(self):
        self.assertEqual(10, self.sensor.read())

    def test_sensor_read_producer(self):
        self.sensor.producer = SensorProducer()
        self.assertEqual(36.5, self.sensor.read())
        self.assertEqual(36.6, self.sensor.read())

if __name__ == '__main__':
    unittest.main()
