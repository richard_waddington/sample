class MockSensor(object):
    def __init__(self, sensor_id, producer=None):
        self.sensor_id = sensor_id
        self.producer = producer
        self.def_value = 10

    def read(self):
        if self.producer:
            return self.producer.value
        else:
            return self.def_value
