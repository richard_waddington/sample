import re
import socketserver
import threading
from sample.sensors import MockSensor
from .producer import SensorProducer


class TstTCPHandler(socketserver.StreamRequestHandler):
    def handle(self):
        """ handle inbound command and return response from the framework """

        self.data = self.rfile.readline().strip()
        response = self.server.framework.do_command(self.data)
        self.wfile.write(response)


# TODO: replace regex with a robust command parser - there will likely be many more commands -
# GET-STATE, GET-LAST_ERROR, et. al.
VALID_COMMANDS = re.compile(b'^GET-SENSOR,\d+$|^SET-SENSOR,\d+,[-+]?\d+(?:\.\d+)?$')


class TstFramework(object):
    """ Test Framework class """
    thread = None
    server = None

    def __init__(self):
        self._sensors = [
            MockSensor(0, SensorProducer()),
        ]

    @property
    def sensors(self):
        return self._sensors

    def do_command(self, command):
        """ Parses command, [does it,] and returns the response """

        # Note both the command and the regex are bytes
        m = VALID_COMMANDS.match(command)
        if m:
            cmdstr = m.group(0).decode('utf-8')
            cmd_parts = cmdstr.split(',')
            result = None
            if cmd_parts[0] == 'GET-SENSOR':
                result = self._do_get_sensor(cmd_parts[1])
            elif cmd_parts[0] == 'SET-SENSOR':
                result = self._do_set_sensor(cmd_parts[1], cmd_parts[2])

            return bytes(result, 'utf-8')
        return bytes('NAK: INVALID COMMAND {}'.format(str(command)), 'utf-8')

    def _do_get_sensor(self, sensor_id):
        # reads the value of the sensor
        i = int(sensor_id)
        if i in range(len(self.sensors)):
            msg = 'ACK: GET-SENSOR,{},{}\n'
            return msg.format(sensor_id, self.sensors[i].read())
        else:
            msg = 'NAK: GET-SENSOR,{},ERROR: ID OUT-OF-RANGE\n'
            return msg.format(sensor_id)

    def _do_set_sensor(self, sensor_id, value):
        # sets the value of the sensor in the producer
        i = int(sensor_id)
        if i in range(len(self.sensors)):
            self.sensors[i].producer.value = float(value)
            msg = 'ACK: SET-SENSOR,{},{}\n'
            return msg.format(sensor_id, value)
        else:
            msg = 'NAK: SET-SENSOR,{},ERROR: ID OUT-OF-RANGE\n'
            return msg.format(sensor_id)

    def set_server(self):
        # Only do this once...
        if TstFramework.server is None:
            host = 'localhost'
            port = 9999
            TstFramework.server = socketserver.TCPServer(
                (host, port), TstTCPHandler)
            TstFramework.server.framework = self

    def start_server(self):
        # Only do this once...
        if TstFramework.thread is None:
            server = TstFramework.server
            TstFramework.thread = threading.Thread(
                target=server.serve_forever,
                daemon=True)
            TstFramework.thread.start()
