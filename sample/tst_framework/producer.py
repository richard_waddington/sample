import threading


class SensorProducer(object):
    """ Provides values to a Sensor-like object

    Each time sensor.value is read, it returns the next value in the
    defined sequence. If the end of the sequence is reached, the
    sequence wraps and begins anew.
    """

    def __init__(self):
        # By default, this sensor never fails
        self._default_values = [
            36.5, 36.6, 36.7, 37.0, 37.2, 37.4, 37.6, 38.0,
            38.0, 37.6, 37.4, 37.2, 37.0, 36.7, 36.6, 36.5
        ]
        self._values = self._default_values.copy()
        self._next = 0
        self._lock = threading.Lock()

    @property
    def value(self):
        # value can be read from the main firmware thread
        # and read or modified from the TCP Server thread
        try:
            self._lock.acquire()
            i = self._next % len(self._values)
            self._next += 1
            result = self._values[i]
        finally:
            self._lock.release()
        return result

    @value.setter
    def value(self, new_value):
        # Called by tst_framework to inject a value
        try:
            self._lock.acquire()
            i = self._next % len(self._values)
            self._values[i] = new_value
        finally:
            self._lock.release()
